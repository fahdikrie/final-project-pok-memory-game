Final Project POK — Kelompok 12 (Memory Game)

//===== Anggota Kelompok =====//
* Althof Rafaella Ramdhan
* Fahdii Ajmalal Fikrie
* Muhammad Aulia Adil Murtito


//===== Petunjuk Penggunaan =====//
A. Inisialisasi Program
1. Buka AVRStudio
2. Dalam AVRStudio, buat projek baru, gunakan ATMega8515
3. Copy-Paste file program (MemoryGame.asm/TugasAkhir_Final.txt) ke projek yang baru dibuat
4. Build and Run program (Ctrl+F7)
5. Buka Hapsim
6. Dalam Hapsim, buka konfigurasi xml (TugasAkhir_Final.xml) ke dalam Hapsim hingga muncul konfigurasi Button, LED, dan LCD
7. Kembali ke AVR, Run Program pada AVR (F5) hingga terlihat program pada Hapsim sudah berjalan

B. Cara Bermain
1. Setelah inisialisasi program selesai, akan muncul tulisan "Memory Game, Masukkan Angka: ..."
2. Penantang bisa memasukkan angka 0 s/d 9 menggunakan Button Up (untuk menaikkan angka sebanyak satu kali inkrementasi), dan Button Down (untuk menurunkan angka sebanyak satu kali dekrementasi)
3. Challenger/Penantang bisa memasukkan lebih dari satu angka, dengan memencet Button Confirm pada setiap selesai memilih angka
4. Setelah Penantang selesai memilih angka, pencet Button Start untuk memulai permainan
5. Angka tersebut nantinya akan ditampilkan selama kurang lebih 3 detik
6. Setelah angka ditampilkan, Pemain bisa menebak angka menggunakan Button Up, Down, dan Confirm sama seperti saat Penantang memasukkan angka
7. Setelah menebak dan mengkonfirmasi semua angka, akan muncul hasil pada LED. Apabila jawaban benar semua, maka LED0 s/d LED3 akan menyala dengan warna hijau, apabila jawaban ada yang salah maka LED4 s/d LED7 akan menyala dengan warna merah
8. Program kembali ke layar awal, permainan bisa dimulai kembali
