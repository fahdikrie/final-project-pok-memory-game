.include "m8515def.inc"

.org $00
rjmp START
.org $0E
rjmp InterruptServiceRoutine

START:

initStack:
ldi r16, high(RAMEND)
out SPH, r16
ldi r17, low(RAMEND)
out SPL, r16

/*--------------*/

initLCD:
//Bersihkan layar awal
ldi r16, 1
out PORTA, r16
rcall enable

//Function set: DL(1), N(1), F(0)
ldi r16, 0x38
out PORTA, r16
sbi PORTB, 1
cbi PORTB, 1

/*---------------*/

getKalimatPembuka:

part1:
//Persiapan iterasi ke-2
ldi r17, 11 
sbi PORTD, 0
ldi ZH, high(kalimatPembuka*2)
ldi ZL, low(kalimatPembuka*2)
rcall iterKalimatLCD

part2:
//Pasang DDRAM
cbi PORTD, 0
ldi r16, 0x94
out PORTA, r16
rcall enable

//Persiapan iterasi ke-2
ldi r17, 14
sbi PORTD, 0
adiw ZL, 1
rcall iterKalimatLCD

/*--------------*/

initInput:

initRAMpart1:
ldi YH, 0x00
ldi YL, 0x60

ldi r24, -1
rcall initButtonUpDown
mov r24, r25

/*--------------*/

getReady:
//bersihkan layar awal
ldi r16, 1
out PORTA, r16
rcall enable

//Kalimat Ready
ldi r17, 6
sbi PORTD, 0
ldi ZH, high(ready*2)
ldi ZL, low(ready*2)
rcall iterKalimatLCD
rcall DELAY_LONGER2
rcall DELAY_LONGER2

//Kalimat Go
getGo:
ldi r17, 3
sbi PORTD, 0
rcall iterKalimatLCD
rcall DELAY_LONGER2
//Set Address RAM
ldi YH, 0x00
ldi YL, 0x60

getAngka:
//bersihkan layar awal
ldi r16, 1
cbi PORTD, 0
out PORTA, r16
rcall enable

//Kalimat Angka:
ldi r17, 6
sbi PORTD, 0
adiw ZL, 1
rcall iterKalimatLCD

initTimer:
ldi r16, 0b01
out TCCR0, r16
ldi r16, (1<<OCF0)
out TIFR, r16
ldi r16, (1<<OCIE0)
out TIMSK, r16
ldi r16, 20
out OCR0, r16
sei

/*--------------*/

//Tunggu timer di sini
anjayMabar:
rjmp anjayMabar

/*--------------*/

jawabAngka:
cli 

//Bersihkan Layar
ldi r16, 1
cbi PORTD, 0
out PORTA, r16
rcall enable

//Kalimat Tebak Angka
ldi r17, 12
sbi PORTD, 0
ldi ZH, high(tebak*2)
ldi ZL, low(tebak*2)
rcall iterKalimatLCD

initRAMpart2:
ldi YH, 0x01
ldi YL, 0x50

rcall initButtonUpDown

checkInput:
ldi XH, 0x01
ldi XL, 0x50 
ldi YH, 0x00
ldi YL, 0x60

loop:
ld r16, X+
ld r17, Y+

cpi r25, 0
breq jumpGreen
dec r25

cp r16, r17
breq loop
rjmp red

jumpGreen:
rjmp green

/*-----TIMER ROUTINE----*/

InterruptServiceRoutine:
dec r25
ld r17, Y+
ldi r16, 0x30
add r16, r17
out PORTA, r16
rcall enableWord
rcall cursorBack
rcall DELAY_LONGER2
rcall DELAY_LONGER2
rcall DELAY_LONGER2
cpi r25, 0
breq jawabAngka
reti

/*----RCALL----*/

jumpSTART:
rjmp START

getLine:
lpm
adiw ZL, 1
subi r17, 1
ret

enable:
sbi PORTB, 0
cbi PORTB, 0
ret

enableWord:
sbi PORTD, 0
rcall enable
cbi PORTD, 0
ret

cursorBack:
ldi r18, 0x10
out PORTA, r18
rcall enable
ret

incLCDNumber:
subi r16, -1
cpi r16, 0x3A
breq setNumberZero
out PORTA, r16
rcall enableWord
ret

decLCDNumber:
subi r16, 1
cpi r16, 0x2F
breq setNumberNine
out PORTA, r16
rcall enableWord
ret

checkUp:
cpi r17, 10
breq setRegZero
ret

checkDown:
cpi r17, -1
breq setRegNine
ret

/*-----DELAY-----*/

DELAY:
ldi  r18, 52
ldi  r19, 242
L1: dec  r19
brne L1
dec  r18
brne L1
nop
ret

DELAY_LONGER:
ldi  r18, 208
ldi  r19, 202
L2: dec  r19
brne L2
dec  r18
brne L2
nop
ret

DELAY_LONGER2:
    ldi  r18, 244
    ldi  r19, 130
    ldi  r20, 6
L3: dec  r20
    brne L3
    dec  r19
    brne L3
    nop 
	ret

/*-----RJMP-----*/

return:
ret

;forever:
;rjmp forever

iterKalimatLCD:
rcall getLine
out PORTA, r0
rcall enable
cpi r17, 0
breq return
rjmp iterKalimatLCD

setNumberZero:
subi r16, 11
rjmp incLCDNumber

setNumberNine:
subi r16, -11
rjmp decLCDNumber

setRegZero:
ldi r17, 0
rjmp checkUp

setRegNine:
ldi r17, 9
rjmp checkDown

stopInput:
rjmp checkInput

green: 
ldi r21,0x04	
out PORTC,r21
rjmp jumpSTART

forever:
rjmp forever

red:
ldi r21,0x02	
out PORTC,r21
rjmp jumpSTART

/*-----BUTTON UP DOWN ---*/

initButtonUpDown:
ldi r17, 0
ldi r16, 0x30
out PORTA, r16
rcall enableWord
rcall cursorBack

getInputConfirm:
sbis PINB, 1
rjmp getInputUp
st Y+, r17
rcall DELAY_LONGER
inc r25
dec r24
cpi r24, 0
breq stopInput
rjmp getInputConfirm

getInputUp:
sbis PINB, 2
rjmp getInputDown
subi r17, -1
rcall checkUp
rcall incLCDNumber
rcall cursorBack
rcall DELAY
rjmp getInputUp

getInputDown:
sbis PINB, 3
rjmp getInputStart
subi r17, 1
rcall checkDown
rcall decLCDNumber
rcall cursorBack
rcall DELAY
rjmp getInputDown

getInputStart:
sbis PINB, 4
rjmp getInputConfirm
ret

/*----PROGRAM MEMORY----*/
kalimatPembuka:
.db "MEMORY GAME"
.db	"MASUKAN ANGKA:"

ready:
.db "READY "
.db "GO!"
.db "ANGKA:"

tebak:
.db "TEBAK ANGKA:"
